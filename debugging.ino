void printDebugln(String message, int value){
    if(printDebug){
    Serial.print(message);
    Serial.print(": ");
    Serial.println(value);
  }
}

void printDebugln(String message, float value){
    if(printDebug){
    Serial.print(message);
    Serial.print(": ");
    Serial.println(value);
  }
}

void printDebugln(String message){
    if(printDebug){
    Serial.println(message);
  }
}
