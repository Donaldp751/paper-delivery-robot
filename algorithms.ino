void printDebugln(String message, int value);

enum NavState{
  standby,
  navigateToPrinterOne,
  navigateToPrinterTwo
};

enum LineFollowMode{
  followLeft,
  followCenter,
  followRight
};

struct PIDGains{
float Throttle;
float P;
float I;
float D;
};

NavState GlobalNavState = standby;
NavState LastGlobalNavState = GlobalNavState;

PIDGains *unloadedTerms;
PIDGains *loadedTerms;
PIDGains *waitingTerms;

float P_Error = 0;
float Last_P_Error = 0;
float I_Error = 0;
float D_Error = 0;

int turnThresh = 30;
bool turnAroundComplete = false;

void setupPID(){
  //the robot is heavier with the ream of paper so the tuning is slightly different
  //to account for the heavier dynamics and lessened response to controls
  loadedTerms = new PIDGains;
  loadedTerms->Throttle = 650;
  loadedTerms->P = 75;
  loadedTerms->I = 25;
  loadedTerms->D = -400;

  //when the robot is returning without the paper it is significantly lighter thus
  //requring slightly different tuning 
  unloadedTerms = new PIDGains;
  unloadedTerms->Throttle = 500;
  unloadedTerms->P = 70;
  unloadedTerms->I = 15;
  unloadedTerms->D = -350;

  waitingTerms = new PIDGains;
  waitingTerms->Throttle = 0;
  waitingTerms->P = 10;
  waitingTerms->I = 15;
  waitingTerms->D = -80;
}

//Mixing alg based on guide found here http://home.kendra.com/mauser/joystick.html
void MotorControl(float _throttle, float _steering)
  { 
  float _M1Output;
  float _M2Output;

  float _V = (controlRange - abs(_steering)) * (_throttle / controlRange) + _throttle;
  float _W = (controlRange - abs(_throttle)) * (_steering / controlRange) + _steering;

  _M1Output = (_V - _W) / 2;
  _M2Output = (_V + _W) / 2;
  SetMotorSpeed(_M1Output, _M2Output);
  }

bool withinThresh(int low, int high, int val){
  if(val >= low && val <= high)
    return true;
  return false;
}

int lastRadarReading = 0;
int handleRadar(int dist, int lower, int upper){
  if(withinThresh(lower,upper,lastRadarReading) && withinThresh(lower,upper,dist))
    return true;
    
  if(withinThresh(lower,upper,dist))
    {
      lastRadarReading = dist;
    }
    
  return false;
}

bool followTape(struct PIDGains *gain){
int tapePosition = getLinePos();
float Steering = CalculateControl(tapePosition,gain);

  if(tapeDetect and Steering != 0)
    MotorControl(gain->Throttle, Steering);
  else if(Steering == 0 and tapeDetect)
    MotorControl(gain->Throttle, 0);
  else {
    MotorControl(0, 0);
    return false;
  }
  return true;
}


int lastError = 0;
int CalculateControl(int error, struct PIDGains *gain){
  P_Error = error * gain->P;
  D_Error = (lastError - error) * gain->D;
  I_Error = (I_Error + error * gain->I)* .5;
  lastError = error;
  int output = (P_Error) + (I_Error) + (D_Error);

  if(printDebug){
    Serial.print("P_Error: ");
    Serial.println(P_Error);
    Serial.print("I_Error: ");
    Serial.println(I_Error);
    Serial.print("D_Error: ");
    Serial.println(D_Error);
  }
  return output;
}



int getLinePos()
{
  updateLineData();
  bool isDetectionError = false;

  int tapeLeft = 0;
  int tapeRight = 0;

  for(int i = 0; i < 8; i++){
  if(LineFollowerModuleRaw[14 - i*2] > tapeThreshold)
    tapeRight ++;
  else
    break;
  }
  
  for(int i = 0; i < 8; i++){
  if(LineFollowerModuleRaw[i*2] > tapeThreshold)
    tapeLeft ++;
  else
    break;
  }
 if(printDebug){
  Serial.print("tapeLeft:");
  Serial.println(tapeLeft);
  Serial.print("tapeRight:");
  Serial.println(tapeRight);
 
  if(tapeLeft > tapeRight)
    Serial.println("tape left");
  else if(tapeLeft < tapeRight)
    Serial.println("tape right");
  else
    Serial.println("tape centered");
  
  Serial.print("OtherError = ");
  Serial.println(tapeLeft-tapeRight);
  }
    //endDetected
  bool endDetection = true;

  for(int i = 0; i < 7; i++){
    dervReadings[i] = abs(LineFollowerModuleRaw[i*2] - LineFollowerModuleRaw[i*2 + 2]);
    isDetectionError |= dervReadings[i] > 45;
    endDetection &= LineFollowerModuleRaw[i*2] < tapeThreshold;
  }
  endDetected = endDetection;

  tapeDetect = isDetectionError;

  if(printDebug){
    Serial.print("end detection: ");
    Serial.println(endDetected);
    
    Serial.print("tapeDetect");
    Serial.println(tapeDetect);
  }
//branchDetectionCounter

    if((state == ClosePrinterNavigation || state == FarPrinterReturnNavigation) &&  branchDetectionCounter > 0)
    {
      branchDetectionCounter --;
      return (tapeLeft - 3) * 2;
    }
  else if((state == ClosePrinterReturnNavigation && state == FarPrinterNavigation) && branchDetectionCounter > 0) {
      branchDetectionCounter --;
      return (3 - tapeRight) * 2;
    }


  return tapeLeft -tapeRight;
}

int turnAroundCounter = 0;
void TurnRobotAround(){
  if(turnAroundCounter < turnThresh){
    MotorControl(0, 400);
    turnAroundCounter++;
  }
  else{
    getLinePos();
    MotorControl(250, -100);
    if(tapeDetect)
      turnAroundComplete = true;
    }
  }
