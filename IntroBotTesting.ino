#include "constants.h" //contains defines like pins and control range shouldn't change unless hardware changes

/*
float DefaultThrottle = 600;   // -1000 to 1000  Reverse - Forward
float P_Gain = 75;
float I_Gain = 25;
float D_Gain = -400;//-200;
*/

enum stateMachine{
  initRobot,
  waitingForStart,
  ClosePrinterNavigation,
  FarPrinterNavigation,
  UnloadingPaper,
  TurnAround,
  ClosePrinterReturnNavigation,
  FarPrinterReturnNavigation,
  obstacleDetected,
  endRun
};

stateMachine state = initRobot;
stateMachine lastState = initRobot;
stateMachine lastTransistionState = initRobot;
stateMachine lastNavState = initRobot;

int muteMotors = 1; //if muteMotors = 0 the output is disabled
int tapeThreshold = 150;
int mainLoop_ms = 75; //Main loop delay in milli seconds

int branchDetectionCounter = 0;

bool printDebug = 0; //debug is disabled by default, send "1" in the console to enable and "0" to disable

unsigned char LineFollowerModuleRaw[16];
bool tapeDetect = false;

int dervReadings[7];

bool endDetected = 0;
//bool returningToHome = false;

void serialEvent(){
  byte req = Serial.read();
  if(req == '1'){
    printDebug = 1; //enable printing debug statements
  }
  else if(req == '0'){
    printDebug = 0; //turn off printing debug statements
  }
}

void setup() {
  Serial.begin(115200); //init serial
  Serial.println("Starting");
  //Setting motor direction pins to output
  pinMode(M1DirPin, OUTPUT);
  pinMode(M2DirPin, OUTPUT);

  pinMode(ultraTrig, OUTPUT);
  pinMode(ultraEcho, INPUT);

  //FOR SECOND ULTRASONIC
  pinMode(secondUltraTrig, OUTPUT);
  pinMode(secondUltraEcho, INPUT);
  pinMode(secondUltraVCC, OUTPUT);
  pinMode(secondUltraGND, OUTPUT);
  digitalWrite(secondUltraVCC, HIGH);
  digitalWrite(secondUltraGND, LOW);
  
  
  pinMode(pushButton, INPUT_PULLUP);
  
  pinMode(redLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
  pinMode(greenLED, OUTPUT);

  pinMode(paperSwitch, INPUT_PULLUP);

  Wire.begin();        // join i2c bus (address optional for master)
}

void RunStateMachine();

void loop() {
  RunStateMachine();
  delay(mainLoop_ms);
  }
