void printDebugln(String message, int value);

int getUltrasonicDistance(int trig, int echo)
{
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  int duration = pulseIn(echo, HIGH,25000);
  int distance= duration*0.034/2;

  printDebugln("ultrasonic distance", distance);

  return distance;
}


//Takes the motor speeds in the range of -1000 to 1000 and converts to 8 bit pwm and direction for the motors
void SetMotorSpeed(float _M1Speed, float _M2Speed)
{
  //Get motor directions
  bool _M1Dir = _M1Speed <= 0;  
  bool _M2Dir = _M2Speed >= 0;

  //Get Positive value for pwm and scale to 8 bit range
  int _M1Output = abs(_M1Speed)/4;
  int _M2Output = abs(_M2Speed)/4;

  if(_M1Output > 255) _M1Output = 255;
  else if(_M1Output > 10 && _M1Output < 80) _M1Output = 80;
  
  if(_M2Output > 255) _M2Output = 255;
  else if(_M2Output > 10 && _M2Output < 80) _M2Output = 80;
  
  //Write pwm (speed) value to motor controller
  analogWrite(M1SpeedPin, (_M1Output * muteMotors));  
  analogWrite(M2SpeedPin, (_M2Output * muteMotors));
  
  //Write motor directions to motor controller
  digitalWrite(M1DirPin, _M1Dir);
  digitalWrite(M2DirPin, _M2Dir);
}

bool paperDetected(){
  return digitalRead(paperSwitch);
}

bool startPBDetected(){
  return !digitalRead(pushButton);
}

void updateLineData(){
  unsigned char i2cIterator = 0;

 Wire.requestFrom(9, 16);    // request 16 bytes from slave device #9
  //Serial.println("request complete");
  while (Wire.available())   // slave may send less than requested
  {
    //Serial.println("reading");
    if(i2cIterator % 2 == 0){
      LineFollowerModuleRaw[i2cIterator] = Wire.read(); // receive a byte as character and save for usage
      printDebugln("LineFollowerModule", LineFollowerModuleRaw[i2cIterator]);
    }
    else
      Wire.read();  //dont save msb because it isnt used

    if (i2cIterator < 15)
      i2cIterator++;
    else
      i2cIterator = 0;
  }
}

void flashLED(int pin, int delayms, int quantity){
  while(quantity > 0){
    digitalWrite(pin,1);
    delay(delayms);
    digitalWrite(pin,0);
    delay(delayms);
      quantity--;
    } 
  }
