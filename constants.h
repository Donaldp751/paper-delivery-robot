#include <Wire.h>

#define pushButton 2
#define yellowLED 3
#define ultraTrig 5 //ultrasonic sensors trigger
#define ultraEcho 6 //ultrasonic echo 
#define paperSwitch 7 //limit switch to detect paper 
#define redLED 8
#define greenLED 9
#define M1SpeedPin 10
#define M2SpeedPin 11
#define M1DirPin 12
#define M2DirPin 13

#define secondUltraGND 14
#define secondUltraVCC 17
#define secondUltraTrig 16
#define secondUltraEcho 15

#define controlRange 1000