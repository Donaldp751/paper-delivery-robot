

int obstacleDetectionCounter = 0;
int obstacleWaitTime = 5000;



void initStateMachine(){
    setupPID();

}

void followTapeTuned(bool loaded){
  if(loaded){
    followTape(loadedTerms); 
  }
  else{
    followTape(unloadedTerms); 
  }
}

void RunStateMachine(){
updateState();

switch(state)
{  
  case initRobot:
    initStateMachine();
    state = waitingForStart;
    break;
  case waitingForStart:
    followTape(waitingTerms);
    break;
  case ClosePrinterNavigation:
    if(turnAroundComplete)
      state = ClosePrinterReturnNavigation;
    followTapeTuned(paperDetected());
    break;
  case ClosePrinterReturnNavigation:
    followTapeTuned(paperDetected());
    break;
  case FarPrinterNavigation:
    if(turnAroundComplete)
      state = FarPrinterReturnNavigation;
    followTapeTuned(paperDetected());
    break;
  case FarPrinterReturnNavigation:
    followTapeTuned(paperDetected());
    break;
  case UnloadingPaper:
    followTape(waitingTerms);
    turnAroundComplete = false;
    break;
  case TurnAround:
    TurnRobotAround();
    break; 
  case obstacleDetected:
    followTape(waitingTerms);
    break;
  case endRun:
    delay(500);
    turnAroundComplete = false;
    state = waitingForStart;
    printDebugln("End of run");
    break;
}


if(lastState != state){
  lastTransistionState = lastState;
  if (isNavigationState()){
    lastNavState = state;
  }
}
lastState = state;
printDebugln("state",state);
}
  
void updateState(){

  if(withinThresh(5,20,getUltrasonicDistance(secondUltraTrig, secondUltraEcho))){
    
    branchDetectionCounter = 50;
  }

  if(startPBDetected()){
    if(state == waitingForStart){
      int counter = 0;
      while(!digitalRead(pushButton)){
        counter ++;
        delay(1000);
      }
      if(counter >= 2)
        {
        state = FarPrinterNavigation;
        
        do{
          flashLED(greenLED, 200, 5);
          delay(1000);
        }while(!paperDetected());
        
        }
      else if(counter >= 1){
        state = ClosePrinterNavigation;
        do{
          flashLED(greenLED, 200, 2);
          delay(1000);
          }while(!paperDetected());
        }
      }
  else{
    state = endRun;
    flashLED(redLED, 200, 4);
    }
  }
   if(isNavigationState() &&  handleRadar(getUltrasonicDistance(ultraTrig, ultraEcho),10,50))
  {
    state = obstacleDetected;
    obstacleDetectionCounter = 0;
  }
  else if(state == obstacleDetected){
    if((obstacleWaitTime / mainLoop_ms) > obstacleDetectionCounter)
      obstacleDetectionCounter ++;
    else
      state = lastTransistionState;
  }

   if((state == ClosePrinterNavigation || state == FarPrinterNavigation) && endDetected && branchDetectionCounter < 5){
      state = UnloadingPaper;
   }
   else if(state == UnloadingPaper){
      if(!paperDetected()){
        delay(3000);
        turnAroundCounter = 0;
        state = TurnAround;
      }
   }
   else if(state == TurnAround && turnAroundComplete){
    state = lastNavState;
   }
}

bool isNavigationState(){
  return (state == ClosePrinterNavigation || state == FarPrinterNavigation  || state == ClosePrinterReturnNavigation  || state == FarPrinterReturnNavigation);
}
